from project import db, app
from project.models import Role, User
from project import user_datastore
from flask_security.utils import encrypt_password


# insert data
db.create_all()

with app.app_context():
  user_role = Role(name='user')
  super_user_role = Role(name='superuser')

  test_user = user_datastore.create_user(
      email='',
      password=encrypt_password(''),
      roles=[user_role, super_user_role]
  )
# commit the changes
db.session.commit()