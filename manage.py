# -*- coding: utf-8 -*-

from flask_script import Manager, Server
from flask_migrate import Migrate, MigrateCommand
import os
from config import config
from project import app, db, user_datastore
from project.models import Role, User, Combo
from flask_security.utils import encrypt_password
from flask_restless import APIManager

app.config.from_object(os.getenv('FLASK_CONFIG') or config['development'])

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)
manager.add_command("runserver", Server(port=8080))

restless = APIManager(app, flask_sqlalchemy_db=db)

restless.create_api(Combo, methods=['GET', 'POST', 'DELETE'])
restless.create_api(User, methods=['GET', 'PUT'], allow_functions=True)

if __name__ == '__main__':
  """with app.app_context():
    super_user_role = Role(name='superuser')

    test_user = user_datastore.create_user(
        email='admin@grace.cat',
        password=encrypt_password('linkii'),
        roles=[super_user_role]
    )
    db.session.commit()"""
  manager.run()


  """select Combos.id, Combos.data, Images.filename as image_file, Audio.filename as audio_file,
    coalesce(view_count, 0) as view_count,
    coalesce(correct_count, 0) as correct_count,
    coalesce(streak_count, 0) as streak_count
    from Combos
    join Images on (Combos.image_id = Images.id)
    left join Audio on (Combos.id = Audio.combo_id)
    left outer join user_combos on (user_combos.user_id = 1 and user_combos.combo_id = combos.id)
    where round = 1 and proficiency = 'A2' and Combos.content_group = 'nonpro'
    order by view_count desc;"""
