# -*- coding: utf-8 -*-
import os

class BaseConfig(object):
  DEBUG = True
  WTF_CSRF_ENABLED = False
  SECRET_KEY = os.getenv('SECRET_KEY') or 'lalalalalalalalalalalalalala'
  SQLALCHEMY_DATABASE_URI = 'postgres:///kermp'

  #Security configgies
  SECURITY_LOGIN_WITHOUT_CONFIRMATION = True
  SECURITY_CONFIRMABLE = False
  SECURITY_CHANGEABLE = False
  SECURITY_TRACKABLE = False
  SECURITY_RECOVERABLE = True
  SECURITY_REGISTERABLE = True
  SECURITY_SEND_REGISTER_EMAIL = False
  SECURITY_PASSWORD_HASH = 'pbkdf2_sha512'
  SECURITY_PASSWORD_SALT = os.getenv('SECURITY_PASSWORD_SALT') or 'pretzelBretzelBabesodiummmmmclhoride'
  SECURITY_POST_LOGIN_VIEW = '/home'
  SECURITY_POST_REGISTER_VIEW = '/home'

  MAIL_SERVER = 'smtp.gmail.com'
  MAIL_PORT = 465
  MAIL_USE_SSL = True
  MAIL_DEFAULT_SENDER = "Billy"
  SECURITY_EMAIL_SENDER = "billy@brainbilly.com"
  MAIL_USERNAME = os.environ.get('MAIL_USERNAME') or 'poop'
  MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD') or 'apple, poutine, and chocolate thingies'

  GOOGLE_API_KEY = os.environ.get('GOOGLE_API_KEY') or "schwwwoppp"
  MAILCHIMP_API_KEY = os.environ.get('MAILCHIMP_API_KEY') or "bloops"
  BITLY_ACCESS_TOKEN = os.environ.get('BITLY_ACCESS_TOKEN') or "lilbit"

  #site flash / error messaging
  SECURITY_MSG_ALREADY_CONFIRMED = (u'Votre adresse a déjà été confirmée', 'info')
  SECURITY_MSG_CONFIRMATION_REQUEST = (u'Merci de confirmer votre adresse mail', 'info')
  SECURITY_MSG_CONFIRM_REGISTRATION = (u'Merci. Un mail de confirmation été envoyé à l\'adresse %(email)s', 'success')
  SECURITY_MSG_EMAIL_ALREADY_ASSOCIATED = (u'Adresse mail déjà utilisée', 'error')
  SECURITY_MSG_EMAIL_CONFIRMED = (u'Votre adresse a été confirmée.', 'success')
  SECURITY_MSG_EMAIL_NOT_PROVIDED = (u'Adresse mail non fourni', 'error')
  SECURITY_MSG_INVALID_CONFIRMATION_TOKEN = (u'Lien de confirmation invalide', 'error')
  SECURITY_MSG_INVALID_EMAIL_ADDRESS = (u'Adresse mail invalide', 'error')
  SECURITY_MSG_INVALID_LOGIN_TOKEN = (u'Lien de connexion invalide', 'error')
  SECURITY_MSG_INVALID_PASSWORD = (u'Mot de passe invalide', 'error')
  SECURITY_MSG_INVALID_RESET_PASSWORD_TOKEN = (u'Lien de réinitialisation invalide', 'error')
  SECURITY_MSG_LOGIN = (u'Authentification requise. Veuillez vous connecter.', 'info')
  SECURITY_MSG_LOGIN_EMAIL_SENT = (u'Lien de connexion envoyé', 'success')
  SECURITY_MSG_PASSWORD_CHANGE = (u'Votre mot de passe a été modifié', 'success')
  SECURITY_MSG_PASSWORD_INVALID_LENGTH = (u'Votre mot de passe doit contenir au moins 6 caractères.', 'error')
  SECURITY_MSG_PASSWORD_IS_THE_SAME = (u'Vous devez saisir un mot de passe différent du précédent.', 'error')
  SECURITY_MSG_PASSWORD_MISMATCH = (u'Le mot de passe ne correspond pas avec l\'adresse mail.' , 'error')
  SECURITY_MSG_PASSWORD_NOT_PROVIDED = (u'Mot de passe non fourni', 'error')
  SECURITY_MSG_PASSWORD_RESET = (u'Votre mot de passe a été réinitialisé.', 'success')
  SECURITY_MSG_PASSWORD_RESET_REQUEST = (u'Un mail de réinitialisation a été envoyé à l\'adresse %(email)s', 'info')
  SECURITY_MSG_RETYPE_PASSWORD_MISMATCH = (u'Les mots de passe ne correspondent pas.', 'error')
  SECURITY_MSG_UNAUTHORIZED = (u'Page inaccessible', 'error')
  SECURITY_MSG_USER_DOES_NOT_EXIST = (u'Nom d\'utilisateur invalide', 'error')

  SECURITY_EMAIL_SUBJECT_REGISTER = u'Bienvenue chez Brain Billy !'
  SECURITY_EMAIL_SUBJECT_PASSWORD_NOTICE = u'Votre mot de passe a été réinitialisé '
  SECURITY_EMAIL_SUBJECT_PASSWORD_RESET = u'Comment modifier votre mot de passe'
  SECURITY_EMAIL_SUBJECT_PASSWORD_CHANGE_NOTICE = u'Votre mot de passe a été modifié '
  SECURITY_EMAIL_SUBJECT_CONFIRM = u'Merci de confirmer votre adresse mail'


  ##SECURITY_MSG_CONFIRMATION_EXPIRED = (, 'error')
  ##SECURITY_MSG_CONFIRMATION_REQUIRED = (, 'error')
  ##SECURITY_MSG_DISABLED_ACCOUNT = (, 'error')
  ##SECURITY_MSG_INVALID_REDIRECT = (, 'error')
  ##SECURITY_MSG_LOGIN_EXPIRED = (, 'error')
  ##SECURITY_MSG_PASSWORDLESS_LOGIN_SUCCESSFUL = (, 'success')
  ##SECURITY_MSG_PASSWORD_NOT_SET = (, 'error')
  ##SECURITY_MSG_PASSWORD_RESET_EXPIRED = (, 'error')
  ##SECURITY_MSG_REFRESH = (, 'info')

class DevConfig(BaseConfig):
  DEBUG = True


class DevLiveConfig(BaseConfig):
  DEBUG = True
  SECURITY_LOGIN_WITHOUT_CONFIRMATION = True
  SECURITY_CONFIRMABLE = True
  SECURITY_CHANGEABLE = False
  SECURITY_TRACKABLE = True
  SECURITY_RECOVERABLE = True
  SECURITY_REGISTERABLE = True
  SECURITY_SEND_REGISTER_EMAIL = True

  #SQLALCHEMY_DATABASE_URI = 'postgres://SwellyDeli:'+ (os.getenv('AWS_DATABASE_PASSWORD') or '') +'@swellydeli.cvap5tju5wvk.eu-west-1.rds.amazonaws.com:5432/swellish'


class ProdConfig(BaseConfig):
  DEBUG = False
  SECURITY_LOGIN_WITHOUT_CONFIRMATION = True
  SECURITY_CONFIRMABLE = True
  SECURITY_CHANGEABLE = False
  SECURITY_TRACKABLE = True
  SECURITY_RECOVERABLE = True
  SECURITY_REGISTERABLE = True
  SECURITY_SEND_REGISTER_EMAIL = False

  #SQLALCHEMY_DATABASE_URI = 'postgres://SwellyDeli:'+ (os.getenv('AWS_DATABASE_PASSWORD') or '') +'@swellydeli.cvap5tju5wvk.eu-west-1.rds.amazonaws.com:5432/swellish'


config = {
    'development': DevConfig,
    'devlive': DevLiveConfig,
    'production': ProdConfig,
    'default': DevConfig
}
