# -*- coding: utf-8 -*-

from flask import after_this_request, make_response, render_template, redirect, url_for, request, session, flash, Blueprint
from flask_login import login_required
from flask_security import current_user
from project import db, app, mail
from project.models import User
from hashlib import md5
import requests
from datetime import datetime
from flask_mail import Message
from json import dumps

home_bp = Blueprint(
    'home', __name__,
    template_folder='templates'
)

@home_bp.route("/sendfeedback", methods=['POST'])
def sendfeedback():
  data = request.args['data']
  subject = request.args['subject']
  body = request.args['message']

  recipients = ['link@grace.cat']
  date = datetime.now()

  msg = Message(subject=subject, recipients=recipients, body=body)
  msg.attach(filename="gameUserData{0}.txt".format(date), content_type='text/tab-separated-values', data=data)
  mail.send(msg)
  return make_response(dumps(''), 200)

@home_bp.route("/unsubscribe")
def unsubscribeFromMailchimp():
  email = request.args['user']
  md5mail = md5(email.lower()).hexdigest()
  r = requests.delete('https://us12.api.mailchimp.com/3.0/lists/a189ed4b74/members/{0}'.format(md5mail), auth=('gelink', app.config['MAILCHIMP_API_KEY']))
  return redirect(url_for('home.home'))

@home_bp.route("/referral")
def referral_naked():
  return redirect(url_for('security.register'))

from werkzeug.local import LocalProxy
from flask import current_app
from flask_security.views import _ctx, _commit, _render_json
from flask_security.registerable import register_user
from flask_security.utils import login_user, get_post_register_redirect

_security = LocalProxy(lambda: current_app.extensions['security'])

@home_bp.route("/referral/<token>", methods=['GET', 'POST'])
def referral(token):
  """View function which handles a registration request."""
  if _security.confirmable or request.json:
    form_class = _security.confirm_register_form
  else:
    form_class = _security.register_form

  if request.json:
    form_data = MultiDict(request.json)
  else:
    form_data = request.form

  form = form_class(form_data)

  if form.validate_on_submit():
    user = register_user(**form.to_dict())
    form.user = user

    if not _security.confirmable or _security.login_without_confirmation:
      after_this_request(_commit)
      login_user(user)
      record_referral(token)

    if not request.json:
      if 'next' in form:
        redirect_url = get_post_register_redirect(form.next.data)
      else:
        redirect_url = get_post_register_redirect()

      return redirect(redirect_url)
    return _render_json(form, include_auth_token=True)

  if request.json:
    return _render_json(form)

  return render_template("referral.html",
                                   register_user_form=form,
                                   token=token,
                                   **_ctx('register'))

def record_referral(token):
  user = User.query.filter_by(referral_token=token).first()
  if(user):
    unlocked = False
    user.referral_num = user.referral_num + 1
    if user.referral_num >= 5:
      user.locked = False
      unlocked = True
    sendReferralMsg(unlocked, user.email)

def sendReferralMsg(unlocked, recipient):
  if(unlocked):
    #send message congratulation account unlocked
    subject=' Félicitations, vos 5 amis sont inscrits ! '
    htmlTemplate = 'security/email/referral-unlocked.html'
    textTemplate = 'security/email/referral-unlocked.txt'
  else:
    #send message X more friends
    subject=' Votre ami vient de s’inscrire'
    htmlTemplate = 'security/email/referral-locked.html'
    textTemplate = 'security/email/referral-locked.txt'

  print recipient, current_user.email
  msg = Message(subject=subject, recipients=[recipient])
  msg.body = render_template(textTemplate, referee=recipient, friend=current_user.email)
  msg.html = render_template(htmlTemplate, referee=recipient, friend=current_user.email)
  mail.send(msg)

  return make_response(dumps(''), 200)

@home_bp.route("/")
def home():
  if current_user.is_anonymous:
    return render_template("home.html", user=current_user, referral_width=referralWidth())
  return redirect("/home")

def referralWidth():
  if hasattr(current_user, 'referral_num'):
    current_user.referral_num = 5
    if(current_user.referral_num < 1):
      return 0
    elif(current_user.referral_num < 5):
      return 6.5
    elif(current_user.referral_num < 10):
      return 24
    elif(current_user.referral_num < 40):
      return 58.5
    elif(current_user.referral_num < 50):
      return 76
    else:
      return 93
  else:
    return 0



