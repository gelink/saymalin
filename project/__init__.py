# -*- coding: utf-8 -*-

from flask import Flask, redirect, url_for, make_response, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_admin import AdminIndexView, Admin, expose,  helpers as admin_helpers
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required, current_user
from flask_security.utils import encrypt_password
from config import config
from project.forms import ExtendedSendConfirmationForm, ExtendedForgotPasswordForm, ExtendedResetPasswordForm, ExtendedConfirmRegisterForm, ExtendedRegisterForm, ExtendedLoginForm
from flask_mail import Mail

admin = Admin()

import os

app = Flask(__name__)
lm = LoginManager()
lm.init_app(app)

class MyHomeView(AdminIndexView):
  @expose('/')
  def index(self):
    if current_user.has_role('superuser'):
      return self.render('admin/index.html')
    else:
      return redirect(url_for('home.home'))

admin = Admin(
  app,
  name='langiegames',
  base_template='admin_base.html',
  template_mode='bootstrap3',
  index_view=MyHomeView()
)

app.config.from_object(os.getenv('FLASK_CONFIG') or config['production'])

db = SQLAlchemy(app)

mail = Mail(app)

from views import users_bp, home_bp, ndual_bp, myadmin_bp

app.register_blueprint(users_bp)
app.register_blueprint(home_bp)
app.register_blueprint(ndual_bp)
app.register_blueprint(myadmin_bp)

@app.errorhandler(404)
def not_found(error):
    return render_template('404-marcel.html'), 404

from models import User, Word, UserWords, Role, roles_users, \
  Image, Audio, Combo, Translation, SecureModelView, UserCombos

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore, send_confirmation_form=ExtendedSendConfirmationForm, forgot_password_form=ExtendedForgotPasswordForm, reset_password_form=ExtendedResetPasswordForm, confirm_register_form=ExtendedConfirmRegisterForm, register_form=ExtendedRegisterForm, login_form=ExtendedLoginForm)

admin.add_view(SecureModelView(Combo, db.session))
admin.add_view(SecureModelView(Image, db.session))
admin.add_view(SecureModelView(Audio, db.session))
admin.add_view(SecureModelView(Translation, db.session))
admin.add_view(SecureModelView(User, db.session))
admin.add_view(SecureModelView(UserCombos, db.session))

lm.login_view = "users.login"

@lm.user_loader
def load_user(user_id):
  return User.query.filter(User.id == int(user_id)).first()


@security.context_processor
def security_context_processor():
  return dict(
      admin_base_template=admin.base_template,
      admin_view=admin.index_view,
      h=admin_helpers,
  )
