# -*- coding: utf-8 -*-

from project import db

class Audio(db.Model):

  __tablename__ = "audio"

  id = db.Column(db.Integer, primary_key=True)
  filename = db.Column(db.String, nullable=False)
  combo_id = db.Column(db.Integer, db.ForeignKey('combos.id'))

  def __init__(self, filename):
    self.filename = filename

  def __unicode__(self):
        attrs = db.class_mapper(self.__class__).attrs # show also relationships
        if 'filename' in attrs:
            return self.filename
        elif 'code' in attrs:
            return self.code
        else:
            return "<%s(%s)>" % (self.__class__.__name__,
                ', '.join('%s=%r' % (k.key, getattr(self, k.key))
                    for k in sorted(attrs)
                    )
                )

class Image(db.Model):

  __tablename__ = "images"

  id = db.Column(db.Integer, primary_key=True)
  filename = db.Column(db.String, nullable=False, unique=True)
  combo_ids = db.relationship("Combo", back_populates="image")

  def __init__(self, filename):
    self.filename = filename

  def __unicode__(self):
        attrs = db.class_mapper(self.__class__).attrs # show also relationships
        if 'filename' in attrs:
            return self.filename
        elif 'code' in attrs:
            return self.code
        else:
            return "<%s(%s)>" % (self.__class__.__name__,
                ', '.join('%s=%r' % (k.key, getattr(self, k.key))
                    for k in sorted(attrs)
                    )
                )


class Combo(db.Model):

  __tablename__ = "combos"

  id = db.Column(db.Integer, primary_key=True)
  data = db.Column(db.String, nullable=False)
  quiz_data = db.Column(db.String, nullable=True)
  review_data = db.Column(db.String, nullable=True)
  answer_data = db.Column(db.String, nullable=True)
  round = db.Column(db.Integer, nullable=False)
  proficiency = db.Column(db.String(2), nullable=False)
  content_group = db.Column(db.String, nullable=False)
  image_id = db.Column(db.Integer, db.ForeignKey('images.id'))

  image = db.relationship("Image", backref="combos")
  audio_ids = db.relationship("Audio", backref="combo", lazy="dynamic")
  translations = db.relationship("Translation", backref="combo", lazy="dynamic")

  users = db.relationship('UserCombos', back_populates='combo')

  def __unicode__(self):
        attrs = db.class_mapper(self.__class__).attrs # show also relationships
        if 'name' in attrs:
            return self.name
        elif 'data' in attrs:
            return self.data
        else:
            return "<%s(%s)>" % (self.__class__.__name__,
                ', '.join('%s=%r' % (k.key, getattr(self, k.key))
                    for k in sorted(attrs)
                    )
                )


class UserCombos(db.Model):

  __tablename__ = 'user_combos'

  user_id = db.Column(db.Integer, db.ForeignKey('users.id'), primary_key=True)
  combo_id = db.Column(db.Integer, db.ForeignKey('combos.id'), primary_key=True)

  view_count = db.Column(db.Integer, nullable=False)
  correct_count = db.Column(db.Integer, nullable=False)
  streak_count = db.Column(db.Integer, nullable=False)
  last_view_time = db.Column(db.DateTime)
  #first_view_time = db.Column(db.DateTime)
  #gained_time = db.Column(db.DateTime)

  user = db.relationship('User', back_populates="combos")
  combo = db.relationship('Combo', back_populates="users")

  def __init__(self, user, combo, view_count):
    self.user_id = user
    self.combo_id = combo
    self.view_count = view_count
    self.correct_count = 0
    self.streak_count = 0


class Translation(db.Model):

  __tablename__ = "translations"

  id = db.Column(db.Integer, primary_key=True)
  data = db.Column(db.String, nullable=False)
  language = db.Column(db.String, nullable=False)

  combo_id = db.Column(db.Integer, db.ForeignKey('combos.id'))

  def __unicode__(self):
        attrs = db.class_mapper(self.__class__).attrs # show also relationships
        if 'name' in attrs:
            return self.name
        elif 'data' in attrs:
            return self.data
        else:
            return "<%s(%s)>" % (self.__class__.__name__,
                ', '.join('%s=%r' % (k.key, getattr(self, k.key))
                    for k in sorted(attrs)
                    )
                )