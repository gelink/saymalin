# -*- coding: utf-8 -*-

from flask import render_template, redirect, url_for, request, session, flash, Blueprint, make_response, jsonify
from flask_login import login_required
#from flask_uploads import UploadSet, IMAGES, AUDIO
from sqlalchemy import text
from flask_security import current_user
from json import dumps
from project import db, app

from project.models import UserCombos, User

import requests
import os

ndual_bp = Blueprint(
    'ndual', __name__,
    template_folder='templates'
)

@ndual_bp.route("/admin/ndual/secretsecretsecret/shhhhhh")
@login_required
def admin():
  return render_template("ndual_data_add.html")

@app.route("/dual1/<round>/<group>/<count>")
@app.route("/dual2/<round>/<group>/<count>")
@app.route("/home")
@app.route("/phrasebank")
@login_required
def angular(**kwargs):
  return make_response(open('project/templates/index.html').read())


def getTrans(words):
  queries = ""
  transDict = {}

  for i in range(len(words)):
    if len(words[i]) >= 1:
      q = words[i].replace("<j>", "").replace("</j>", "")
      words[i] = q
      queries += "&q=" + q
    else:
      words.pop(i)
  print queries
  #r = requests.get("https://www.googleapis.com/language/translate/v2?target=fr&source=en" + queries + "&key=" + os.environ.get('GOOGLE_API_KEY'))
  #for i in range(len(words)):
  #  transDict[words[i]] = r.json()[u'data'][u'translations'][i][u'translatedText']
  return []#transDict


@app.route("/combomb")
@login_required
def combomb():
  staticImgHost ="" # "https://static.brainbilly.com/images/";
  staticAudoiHost = "http://flashy.kermpany.com/static/audio/"

  id = current_user.id
  # verify group is an available group
  if current_user.locked:
    group = 'demo'
    proficiency = 'A1'
    round = 1
  else:
    group = request.args.get('group') or 'demo'
    proficiency = 'D1' if group == u'demo' else current_user.proficiency
  # verify min and max count val
  count = request.args.get('count') or 4
  try:
    int_count = int(count)
    if int_count <= 3:
      count = 3
    elif int_count >= 16:
      count = 16
  except:
    count = 4

  # verify round
  round = request.args.get('round') or 1
  sql = text('select Combos.id, Combos.data, Combos.review_data, Combos.quiz_data, Combos.answer_data, Images.filename as image_file, Audio.filename as audio_file, ' +
    'coalesce(view_count, 0) as view_count, ' +
    'coalesce(correct_count, 0) as correct_count, ' +
    'coalesce(streak_count, 0) as streak_count ' +
    'from Combos ' +
    'join Images on (Combos.image_id = Images.id) ' +
    'left join Audio on (Combos.id = Audio.combo_id) ' +
    'left outer join user_combos on (user_combos.user_id =' + str(current_user.id) + ' and user_combos.combo_id = combos.id) ' +
    'where round = ' + round + ' and proficiency = \'' + proficiency + '\' and Combos.content_group = \'' + group +
    '\' order by view_count, random() ' +
    'limit ' + str(count) + ';')
  result = db.engine.execute(sql)
  combo_data = []
  queries = []
  for row in result:
    print row
    user_combo = UserCombos.query.filter_by(user_id=current_user.id).filter_by(combo_id=row.id).first()
    if user_combo is None:
      #insert
      db.session.add(UserCombos(current_user.id, row.id, 1))
    else:
      #update
      user_combo.view_count += 1
    db.session.commit()
    queries += row.data.strip().split(' ')
    bgImg = row.image_file #staticImgHost + group + '/' + (row.image_file or '')
    thumbImg = row.image_file #staticImgHost + 'thumb/' + group + '/' + (row.image_file or '')
    combo_data.append({'image_th_file': thumbImg, 'image_file': bgImg, 'audio_file': staticAudoiHost + (row.audio_file or 'char_np_1.m4a'), 'text': row.data, 'id': row.id, 'incorrect_text':row.review_data, 'quiz_text':row.quiz_data, 'answer_text':row.answer_data})
  #trans = getTrans(queries)
  response_data = {"combo_data": combo_data} #, "translations" : trans }
  return make_response(dumps(response_data), 200)

@app.route('/update_user', methods=['POST', 'PUT'])
@login_required
def update_user():
  points = request.args.get('points')
  reps = request.args.get('reps')
  cards = request.args.get('cards')
  gainedIds = request.args.get('gained')
  user = User.query.filter_by(id=current_user.id).first()
  user.points += int(points)
  user.reps += int(reps)
  gainedIdsArr = gainedIds[:-1].split(':')
  if len(gainedIdsArr) > 1:
    for cardId in gainedIdsArr:
      user_combo = UserCombos.query.filter_by(user_id=current_user.id).filter_by(combo_id=cardId).first()
      user_combo.correct_count += 1

  db.session.commit()
  return make_response("success", 200)
