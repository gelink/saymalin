# -*- coding: utf-8 -*-

from marshmallow import Schema, fields

class ImageSchema(Schema):
  id = fields.Int(dump_only)
  filename = fields.Str(required=True)


class AudioSchema(Schema):
  id = fields.Int(dump_only)
  filename = fields.Str(required=True)


class ComboSchema(Schema):
  id = fields.Int(dump_only=True)
  data = fileds.Str(required=True)
  round = fields.Int(required=True)
  proficiency = fields.Str(required=True)
  content_group = field.Str(required=True)
  image = fields.Nested(ImageSchema, only='filename')
  audio = fields.Nested(AudioSchema, many=True, only='filename')