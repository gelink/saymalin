Bienvenue {{ user.email }}!

{% if security.confirmable %}
Merci de cliquer sur le lien pour confirmer votre compte :

{{ confirmation_link }}
{% endif %}