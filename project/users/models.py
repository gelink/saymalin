# -*- coding: utf-8 -*-

from project import db
from base64 import b64encode
import os
from datetime import datetime
from flask_security import UserMixin, RoleMixin

import requests
from requests.auth import HTTPBasicAuth
import json
import random
import string
from project import app

# Define models
roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('users.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('roles.id')))

class Role(db.Model, RoleMixin):
  __tablename__ = "roles"

  id = db.Column(db.Integer(), primary_key=True)
  name = db.Column(db.String(80), unique=True)
  description = db.Column(db.String(255))

  def __unicode__(self):
        attrs = db.class_mapper(self.__class__).attrs # show also relationships
        if 'name' in attrs:
            return self.name
        elif 'code' in attrs:
            return self.code
        else:
            return "<%s(%s)>" % (self.__class__.__name__,
                ', '.join('%s=%r' % (k.key, getattr(self, k.key))
                    for k in sorted(attrs)
                    )
                )

class UserWords(db.Model):

  __tablename__ = "user_words"

  user_id = db.Column(db.Integer, db.ForeignKey('users.id'), primary_key=True)
  word_id = db.Column(db.Integer, db.ForeignKey('words.id'), primary_key=True)

  view_count = db.Column(db.Integer, nullable=False)
  correct_count = db.Column(db.Integer, nullable=False)
  streak_count = db.Column(db.Integer, nullable=False)

  user = db.relationship('User',back_populates="words")
  word = db.relationship('Word',back_populates="users")


class User(db.Model, UserMixin):

  __tablename__ = "users"

  id = db.Column(db.Integer, primary_key=True)
  email = db.Column(db.String(255), nullable=False, unique=True)
  password = db.Column(db.String, nullable=False)
  active = db.Column(db.Boolean, nullable=True)
  confirmed_at = db.Column(db.DateTime, nullable=True)
  last_login_at = db.Column(db.DateTime, nullable=True)
  current_login_at = db.Column(db.DateTime, nullable=True)
  last_login_ip = db.Column(db.String(100), nullable=True)
  current_login_ip = db.Column(db.String(100), nullable=True)
  login_count = db.Column(db.Integer, nullable=True)
  registered_at = db.Column(db.DateTime, nullable=True)
  proficiency = db.Column(db.String(2), nullable=False)
  reps = db.Column(db.Integer, nullable=True)
  points = db.Column(db.Integer, nullable=True)
  referral_token = db.Column(db.String, nullable=False)
  referral_num = db.Column(db.Integer, nullable=True)
  #subscription_type = db.Column(db.Integer, nullable=True)
  #subscription_renewal_day = db.Column(db.DateTime, nullable=True)
  bitly_link = db.Column(db.String, nullable=False)
  locked = db.Column(db.Boolean, nullable=False)

  days_streak = db.Column(db.Integer, nullable=False)

  # many to many User<->Word
  words = db.relationship('UserWords', back_populates='user')
  roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
  combos = db.relationship('UserCombos', back_populates='user')


  def __init__(self, email, password, active, roles, proficiency):
    self.email = email
    self.password = password
    self.active = active
    self.proficiency = proficiency
    self.reps = 0
    self.points = 0
    self.referral_token = ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(12))
    self.referral_num = 0
    self.registered_at = datetime.now()
    self.login_count = 1
    self.roles = roles
    self.days_streak = 1
    self.bitly_link = self.generateBitlyLink(self.referral_token)
    self.locked = True

    self.subscribeToMailChimp(self.email)

  def __repr__(self):
    return '{}-{}'.format(self.email, self.proficiency)

  def generateBitlyLink(self, token):
    r = requests.get("https://api-ssl.bitly.com/v3/shorten?access_token=" + app.config['BITLY_ACCESS_TOKEN'] + "&longUrl=https://www.brainbilly.com/referral/" + token + "&domain=bit.ly&format=txt")
    return r.text[:-1]

  def subscribeToMailChimp(self, email):
    payload = json.dumps({"email_address": email, "status": "subscribed"})
    r = requests.post('https://us12.api.mailchimp.com/3.0/lists/a189ed4b74/members', data=payload, auth=('gelink', app.config['MAILCHIMP_API_KEY']))

class Word(db.Model):

  __tablename__ = "words"

  id = db.Column(db.Integer, primary_key=True)
  data = db.Column(db.String, nullable=False)
  definition = db.Column(db.String, nullable=False)

  users = db.relationship('UserWords', back_populates='word')
