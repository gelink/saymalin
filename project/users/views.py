# -*- coding: utf-8 -*-

from flask import Flask, render_template, redirect, url_for, request, flash, Blueprint, make_response
from forms import LoginForm, RegisterForm
from project.models import User, UserCombos, Combo
from flask_login import login_user, logout_user, login_required
from project import db
from flask_security import current_user
from json import dumps

users_bp = Blueprint(
  'users', __name__,
  template_folder='templates'
)

@users_bp.route('/currentuser')
@login_required
def currentUser():
  return make_response(dumps(
    {
        'email': current_user.email
      , 'reps': current_user.reps
      , 'points': current_user.points
      , 'token': current_user.referral_token
      , 'referrals': current_user.referral_num or 0
      , 'proficiency': current_user.proficiency
      , 'bitly_link' : current_user.bitly_link
      , 'locked' : current_user.locked
    }), 200)


@users_bp.route('/getphrases')
@login_required
def phrasebank():
  user_combos = UserCombos.query.filter_by(user_id=current_user.id).filter(UserCombos.correct_count > 0).all()
  phrase_data = []
  for row in user_combos:
    combo = Combo.query.filter_by(id=row.combo_id).first()
    phrase_data.append({'phrase': combo.data, 'gains': row.correct_count})

  #phrase_data = [{'phrase' : 'grace is great', 'gains' : 2}, {'phrase' : 'grace is bomb', 'gains' : 100}, {'phrase' : 'who is great?', 'gains' : 6 },{'phrase' : 'grace is neat', 'gains' : 20}, {'phrase' : 'grace is grand', 'gains' : 10}, {'phrase' : 'who is grand?', 'gains' : 2 },{'phrase' : 'grace is cool', 'gains' : 3}, {'phrase' : 'grace is best', 'gains' : 100}, {'phrase' : 'who is the bomb?', 'gains' : 6 },{'phrase' : 'grace is weird', 'gains' : 18}, {'phrase' : 'grace is wonderful', 'gains' : 19}, {'phrase' : 'who is the GRACE?', 'gains' : 6 }]
  return make_response(dumps(phrase_data), 200)
