# -*- coding: utf-8 -*-

from flask_wtf import Form
from wtforms import TextField, PasswordField
from wtforms.validators import DataRequired, EqualTo, Length

class LoginForm(Form):
  username = TextField('username', validators=[DataRequired()])
  password = PasswordField('password', validators=[DataRequired()])

class RegisterForm(Form):
  username = TextField('username', validators=[DataRequired(), Length(min=3, max=23)])
  password = PasswordField('password', validators=[DataRequired(), Length(min=6, max=60)])
  confirm = PasswordField('repeat password', validators=[DataRequired(), EqualTo('password', message="learn to type doofus")])
