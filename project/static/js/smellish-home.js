$(function() {
  function attachListeners() {
    var intervalID = window.setInterval(changeReferralProgress, 1100);
    $('.scroll-link').click(function(){
        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 500);
        return false;
    });
    $(window).scroll(function() {
      var scrollHeight = $(document).scrollTop();
      var viewHeight = "innerHeight" in window
               ? window.innerHeight
               : document.documentElement.offsetHeight;
      var headerHeight = $("#navington").height();

      if(viewHeight - (headerHeight / 2) <= scrollHeight) {
        $("#navington").addClass("background-nav");
        $(".nav-logo img").show();
      }
      else {
        $("#navington").removeClass("background-nav");
        $(".nav-logo img").hide();
      }
    });
  }

  function changeReferralProgress() {
    var winnerWidth = $("#progressive").width();
    var width = $($(".progress")[0]).width();
    if(width * 0.2 > winnerWidth) {
      $("#progressive").css("width", "20%")
    }
    else if(width * 0.4 > winnerWidth) {
      $("#progressive").css("width", "40%")
    }
    else if(width * 0.6 > winnerWidth) {
      $("#progressive").css("width", "60%")
    }
    else if(width * 0.8 > winnerWidth) {
      $("#progressive").css("width", "80%")
    }
    else if(width > winnerWidth) {
      $("#progressive").css("width", "100%")
      $("#locket").removeClass("ion-locked");
      $("#locket").addClass("ion-unlocked");
    }
    else {
      $("#progressive").css("width", "0%")
      $("#locket").removeClass("ion-unlocked");
      $("#locket").addClass("ion-locked");
    }
  }

  attachListeners();
});