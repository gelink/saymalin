'use strict';

// Declare app level module which depends on filters, and services
var swellishApp = angular.module('swellishApp', [
  'ngRoute',
  'ngAnimate',
  'ngAudio',
  'ngSanitize',
  'swellishConfig',
  'swellishControllers',
  'swellishDirectives',
  'swellishFilters',
  'swellishServices',
  'ui.bootstrap',
  'ui.router',
  'base64',
  'cfp.hotkeys',
  'flash',
  'validation.match',
  'bm.uiTour',
], function($httpProvider) {
  /** http://victorblog.com/2012/12/20/make-angularjs-http-service-behave-like-jquery-ajax/ **/
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
   /**
   * The workhorse; converts an object to x-www-form-urlencoded serialization.
   * @param {Object} obj
   * @return {String}
   */
  var param = function(obj) {
    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

    for(name in obj) {
      value = obj[name];

      if(value instanceof Array) {
        for(i=0; i<value.length; ++i) {
          subValue = value[i];
          fullSubName = name + '[' + i + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value instanceof Object) {
        for(subName in value) {
          subValue = value[subName];
          fullSubName = name + '[' + subName + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value !== undefined && value !== null)
        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
    }

    return query.length ? query.substr(0, query.length - 1) : query;
  };

  // Override $http service's default transformRequest
  $httpProvider.defaults.transformRequest = [function(data) {
    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
  }];
});

swellishApp.config(['$stateProvider', '$locationProvider', '$urlRouterProvider',
  function($stateProvider, $locationProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'static/partials/home.html',
        controller: 'HomeCtrl',
      })
      .state('phrasebank', {
        url: '/phrasebank',
        templateUrl: 'static/partials/phrase_bank.html',
        controller: 'PhraseBankCtrl',
      })
      .state('dual', {
        url: '/dual1/:round/:group/:count',
        templateUrl: 'static/partials/dual.html',
        controller: 'DualCtrl',
      })
      .state('dual2', {
        url: '/dual2/:round/:group/:count',
        templateUrl: 'static/partials/dual2.html',
        controller: 'DualCtrl',
      });
    $locationProvider.html5Mode(true);
  }]);

var swellishControllers = angular.module('swellishControllers', ['ngCookies']);

swellishControllers.controller('PhraseBankCtrl', ['$scope', 'userService', function($scope, userService) {
  $scope.currentUser = JSON.parse(localStorage.getItem('currentUser'));

  userService.getPhraseBank(function(data) {
    if(data.length < 1) {
      data = [{'phrase' : 'Vous n’avez pas encore de phrase.', 'gains' : 'Jouez pour gagner des points !'}]
    }
    $scope.phrases = data;
  });
}]);

swellishControllers.controller('HomeCtrl', ['$scope', '$location' , '$timeout' , '$http', 'userService', 'emailService', '$window', '$uibModal', function($scope, $location, $timeout, $http, userService, emailService, $window, $uibModal) {
  $scope.currentUser = userService.getCurrentUser(function(data){
    $scope.currentUser = data;
    $scope.rulebookOpen = false;
    $scope.feedbackSuccess = false;
    $scope.feedbackErr = false;
    $scope.feedback = {};
    $scope.feedback.feedObj = '';
    $scope.feedback.feedMsg = '';

    $scope.toggleFeedback = function () {
      $uibModal.open({templateUrl: '/static/partials/modal.html', controller: 'HomeCtrl', scope: $scope});
    };

    $scope.sendFeedback = function () {
      $scope.feedbackSuccess = false;
      $scope.feedbackErr = false;

      var gameUserData = JSON.stringify({
              "location": "home"
            , "user_email": $scope.currentUser.email
            , "useragent" : $window.navigator.userAgent
          });

    emailService.sendFeedback(gameUserData, $scope.feedback.feedMsg, $scope.feedback.feedObj);
      $timeout(function() {
        $scope.feedbackSuccess = true;
        $scope.feedbackSent = true;
      }, 800);
    };

    $scope.playGame = function(game, round, group, count) {
      $location.url('/dual'+round+'/'+round+'/'+group+'/'+count);
    };

    $scope.toSplash = function() {
      $location.url('/');
      $window.location.reload();
    };

    $scope.logout = function() {
      $http.get('/logout')
        .then(function(data) {
          localStorage.clear();
          $location.url('/');
          $window.location.reload();
        }, function(err) {
          console.log("err")
        });
    };
  });
}]);

swellishControllers.controller(
    'DualCtrl'
  , [
      '$scope'
    , '$http'
    , '$cookies'
    , '$location'
    , 'focus'
    , '$timeout'
    , 'comboService'
    , '$state'
    , 'userService'
    , '$stateParams'
    , 'ngAudio'
    , 'hotkeys'
    , '$uibModal'
    , '$window'
    , 'uiTourService'
    , 'emailService'
    , function(
        $scope
      , $http
      , $cookies
      , $location
      , focus
      , $timeout
      , comboService
      , $state
      , userService
      , $stateParams
      , ngAudio
      , hotkeys
      , $uibModal
      , $window
      , TourService
      , emailService ) {

  $scope.explorerOpen = true;
  $scope.translationOpen = false;
  $scope.currentWord = 0;
  $scope.quizResults = [];
  $scope.sounds = [];
  $scope.guess = "";
  $scope.showResults = false;
  $scope.correct = 0
  $scope.group = $stateParams.group;
  $scope.round = $stateParams.round;
  if(!parseInt($stateParams.count)) {
    $scope.count = 4;
  }
  else if($stateParams.count >= 16) {
    $scope.count = 16;
  }
  else if($stateParams.count <= 3) {
    $scope.count = 3;
  }
  else {
    $scope.count = $stateParams.count;
  }
  $scope.imageLoading = true;
  $scope.audioLoading = true;
  $scope.loading = $scope.imageLoading || $scope.audioLoading;
  $scope.wordSet = [{'text':'', 'audio_file': '', 'image_file': 'https://static.brainbilly.com/images/default.jpg'}];
  $scope.quizMode = false;
  $scope.feedbackSuccess = false;
  $scope.feedbackErr = false;
  $scope.splitWords = [];
  $scope.imageSet = [];
  $scope.transitionDist = {'width' : (parseInt($scope.count)*100)+'vw', 'transform' : 'translatex(-' + ($scope.currentWord * 100) + 'vw)'};
  $scope.incorrectAnswer = {'visibility' :  'hidden'};
  $scope.photoSquish = {'width' : ($scope.count > 9 ? (100 / (parseInt($scope.count) + 1)) + "%" : '10%')}
  $scope.buttonable = false;
  $scope.countRetry = JSON.parse(localStorage.getItem("countRetry")) || 0;
  $scope.resumeTour = false;
  $scope.playTour =  (localStorage.getItem('tut') ? JSON.parse(localStorage.getItem('tut')) : true) && $scope.count == 4;
  $scope.tourResumeArray = [3,0,0,4,0,0,6];
  $scope.tourIndex = 0;
  $scope.feedback = {};
  $scope.feedback.feedObj = '';
  $scope.feedback.feedMsg = '';

  $scope.buttonTimeout = function() {
    $scope.buttonable = true;
  };


  $scope.toggleExplorer = function () {
    $scope.explorerOpen = !$scope.explorerOpen;
  };

  $scope.touring = function () {
    if($scope.tourResumeArray[$scope.tourIndex]) {
      TourService.getTour().goTo($scope.tourResumeArray[$scope.tourIndex]);
      $scope.tourIndex++;
    }
    else {
      TourService.getTour().pause();
      $scope.tourIndex++;
    }
  }

  $scope.nextQuizPhoto = function() {
    if($scope.playTour) {
      $scope.touring();
    }
    if($scope.currentWord == 0) {
      $scope.endQuiz();
    }
    else {
      $scope.incorrectAnswer = {'visibility' : 'hidden'};
      $scope.guess = "";
      $scope.guessed = false;
      $scope.guessChecked = false;
      $scope.currentWord -= 1;
      $scope.transitionDist = {'width' : (parseInt($scope.count)*100)+'vw', 'transform' : 'translatex(-' + ($scope.currentWord * 100) + 'vw)'};

      $scope.buttonable = false;
      $timeout($scope.buttonTimeout, 2000);

      focus('quizbox');
    }
  };

  $scope.nextPhoto = function() {
    if($scope.playTour) {
      $scope.touring();
    }
    $scope.quizResults.push(-1);
    $scope.quizMode = $scope.currentWord == $scope.wordSet.length-1;
    $scope.currentWord = $scope.quizMode ? $scope.currentWord : $scope.currentWord + 1;
    $scope.transitionDist = {'width' : (parseInt($scope.count)*100)+'vw', 'transform' : 'translatex(-' + ($scope.currentWord * 100) + 'vw)'};

    if($scope.round == 1) {
      $scope.splitWords = $scope.wordSet[$scope.currentWord].text.split(' ');
    }
    else if($scope.round == 2){
      $scope.splitWords = $scope.wordSet[$scope.currentWord].text.split(' ');
    }

    if(!$scope.quizMode) {
      $timeout($scope.playAudio, 2000);
    }

    $scope.buttonable = false;
    $timeout($scope.buttonTimeout, 2000);

    focus('quizbox');
  };

  $scope.endQuiz = function () {
    $scope.showResults = true;
    localStorage.setItem('tut', false);
  };

  $scope.checkGuess = function () {
    if($scope.round == 1) {
      // First correct answer trimmed edge white space. trimmed extra whitespace between words. to lower case. remove bold tags
      var correctAnswer = [$scope.wordSet[$scope.currentWord]['text'].replace(/<j>/gi,'').replace(/<\/j>/gi, '').toLowerCase().replace(/\s+/g, ' ').trim()];

      // Add to correct answers the sentence minus final punctuation
      if(correctAnswer[0][correctAnswer[0].length-1] == "." || correctAnswer[0][correctAnswer[0].length-1] == "!"  || correctAnswer[0][correctAnswer[0].length-1] == "?") {
        correctAnswer.push(correctAnswer[0].slice(0, -1));
      }

      $scope.quizResults[$scope.currentWord] = correctAnswer.indexOf($scope.guess.toLowerCase().replace(/\s+/g,' ')) >= 0 ? 1 : 0;
      if($scope.quizResults[$scope.currentWord] == 0) {
        $scope.incorrectAnswer = {'visibility' : 'visible'};
      }
      $scope.correct += $scope.quizResults[$scope.currentWord];
    }
    else if($scope.round == 2) {
      $scope.quizResults[$scope.currentWord] = $scope.wordSet[$scope.currentWord].answer_text == $scope.guess.toLowerCase();
      $scope.correct += $scope.quizResults[$scope.currentWord];
    }

    $scope.buttonable = false;
    var time = ($scope.quizResults[$scope.currentWord]) ? 10 : 200;
    $timeout($scope.buttonTimeout, time);

    $scope.guessChecked = true;
  };

  $scope.skip = function () {
    $scope.quizResults[$scope.currentWord] = 0;
    $scope.incorrectAnswer = {'visibility' : 'visible'};
    $scope.guessChecked = true;

    $scope.buttonable = false;
    $timeout($scope.buttonTimeout, 200);
  };

  $scope.backToDash = function () {
    var gainedIds = ''
    for(var i=0; i < $scope.quizResults.length; i++) {
      if($scope.quizResults[i]) {
        gainedIds += $scope.wordSet[i].id+':';
      }
    }
    localStorage.removeItem("countRetry");
    userService.saveUserPoints($scope.correct, $scope.quizResults.length, gainedIds, $scope.count);
    $location.url('/home');
  };

  $scope.nextRound = function () {
    var gainedIds = ''
    for(var i=0; i < $scope.quizResults.length; i++) {
      if($scope.quizResults[i]) {
        gainedIds += $scope.wordSet[i].id+':';
      }
    }
    userService.saveUserPoints($scope.correct, $scope.quizResults.length, gainedIds, $scope.count);
    if($scope.correct == $scope.quizResults.length) {
      localStorage.removeItem("countRetry");
      $location.url('/dual'+$scope.round+"/"+$scope.round+"/"+$scope.group+"/"+(parseInt($scope.count)+1))
    }
    else {
      if($scope.countRetry) {
        localStorage.removeItem("countRetry");
        $location.url('/dual'+$scope.round+"/"+$scope.round+"/"+$scope.group+"/"+(parseInt($scope.count)-1))
      }
      else {
        localStorage.setItem("countRetry", 1);
        $state.reload();
      }
    }
  };

  $scope.setTourPause = function () {
    $scope.resumeTour = true;
    TourService.getTour().pause();
  };

  //this should be broken out as a service i fink
  $scope.loadImages = function () {
    $scope.imgLoaded = 0;
    for(var i = 0; i < $scope.wordSet.length; i++) {
      var img = new Image();
      img.onload = function () {
        $scope.imgLoaded += 1;
        if($scope.imgLoaded == $scope.wordSet.length) {
          $scope.imageLoading = false;
          $scope.loading = $scope.imageLoading || $scope.audioLoading;
          $scope.$apply();
        }
      };
      img.src = $scope.wordSet[i].image_file;
      $scope.imageSet.push(img.src);
    }
  };

  $scope.loadAudio = function () {
    $scope.audioLoaded = 0;
      for(var i = 0; i < $scope.wordSet.length; i++) {
      var audio = new Audio();
      audio.oncanplaythrough = function (e) {
        $scope.audioLoaded += 1;
        if($scope.audioLoaded == $scope.wordSet.length) {
          $scope.audioLoading = false;
          $scope.loading = $scope.imageLoading || $scope.audioLoading;
          $scope.$apply();
        }
      };
      $scope.sounds[i] = ngAudio.load($scope.wordSet[i].audio_file);
      audio.src = $scope.wordSet[i].audio_file;
      var audioElm = audio[0];

      if (audio.readyState > 3) {
        audio.oncanplaythrough;
      }
    }
  };

  $scope.$watch('loading', function(oldVal, newVal) {
    if(!$scope.loading && $scope.currentWord == 0 && $scope.sounds[$scope.currentWord]) {
      console.log("watch");
      $scope.playAudio('watch');
      $scope.buttonable = true;
      if($scope.playTour) {
        TourService.getTour().start();
      }
    }
  });

  $scope.toolTipLocations = function () {
    $('#step-1').css('left', $("#step-1-anchor").offset().left - 5);
    $('#step-1').css('top', $("#step-1-anchor").offset().top - (($("#step-1-anchor").height()) / 2));
    $('#step-2').css('left', $("#step-2-anchor").offset().left - 5);
    $('#step-2').css('top', $("#step-2-anchor").offset().top - (($("#step-2-anchor").height()) / 2));
    $('#step-3').css('left', '50%');
    $('#step-3').css('top', $("#quizbox").offset().top + $("#quizbox").height() + 5);
    $('#step-4').css('left', $("#step-4-anchor").offset().left - 5);
    $('#step-4').css('top', $("#step-4-anchor").offset().top - (($("#step-4-anchor").height()) / 2));

    focus('quizbox');
  }

  $(window).resize(function () {
    $scope.toolTipLocations();
  });

  $scope.playAudio = function (e) {
    console.log(e, "  ", $scope.sounds);
    console.log("uhm", $scope.currentWord);
    $scope.sounds[$scope.currentWord].play();
  };

  comboService.getCombos($scope.group, $scope.round, $scope.count).then(function(data) {
    $scope.wordSet = data.data.combo_data;
    $scope.translations = data.data.translations;
    if($scope.round == 1) {
      $scope.splitWords = $scope.wordSet[$scope.currentWord].text.split(' ');
    }
    if($scope.round == 2) {
      $scope.splitWords = $scope.wordSet[$scope.currentWord].text.split(' ');
    }
    //$scope.$apply();

    $scope.mainPhoto = {
      'background-image': 'url(' + $scope.wordSet[$scope.currentWord].image_file + ');'
    };
    $scope.loadImages();
    $scope.loadAudio();
  });

  $scope.toggleFeedback = function () {
    //debugger;
    $uibModal.open({templateUrl: '/static/partials/modal.html', scope: $scope});
  };

  $scope.sendFeedback = function () {
    $scope.feedbackSuccess = false;
    $scope.feedbackSent = false;

    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    var gameUserData = JSON.stringify({
          "location": "game"
          , "user_email": currentUser.email
          , "img_id": $scope.wordSet[$scope.currentWord].image_file
          , "audio_id": $scope.wordSet[$scope.currentWord].audio_file
          , "useragent" : $window.navigator.userAgent
        });

    emailService.sendFeedback(gameUserData, $scope.feedback.feedMsg, $scope.feedback.feedObj);
    $timeout(function() {
      $scope.feedbackSuccess = true;
      $scope.feedbackSent = true;
    }, 800);
  };

  $scope.enterHandler = function(e, hotkey) {
    if(!$scope.buttonable){
      return;
    }
    else {
      if($scope.showResults) {
        $scope.nextRound();
      }
      else if(!$scope.quizMode){
        $scope.nextPhoto();
      }
      else {
        if($scope.guessChecked) {
          $scope.guessChecked = false;
          $scope.nextQuizPhoto();
        }
        else if($scope.guess.length > 0){
          $scope.checkGuess();
        }
        else {
          return;
        }
      }
    }
  };

  hotkeys.bindTo($scope).add({
    combo: 'enter',
    description: 'Move to next card',
    callback: $scope.enterHandler
  })
  .add({
    combo: 'space',
    description: 'Move to next card',
    callback: $scope.enterHandler
  })
  .add({
    combo: 'left',
    description: 'Move to next card',
    callback: $scope.enterHandler
  })
  .add({
    combo: 'right',
    description: 'Move to next card',
    callback: $scope.enterHandler
  });
}]);

var swellishFilters = angular.module('swellishFilters', []);

swellishFilters.filter('stripEmail', function () {
  return function (item) {
    if(item) {
      return item.split('@')[0];
    }
  };
})
.filter('offset', function () {
  return function (input, start) {
    if (!input) return [];
    start = parseInt(start, 10);
    return input.slice(start);
  };
});

var swellishConfig = angular.module('swellishConfig', []);

var swellishDirectives = angular.module('swellishDirectives', []);

swellishDirectives.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
              });
            }
    };
});

var swellishServices = angular.module('swellishServices', []);

swellishServices.factory('focus', ['$timeout', '$window', function($timeout, $window) {
    return function(id) {
      // timeout makes sure that is invoked after any other event has been triggered.
      // e.g. click events that need to run before the focus or
      // inputs elements that are in a disabled state but are enabled when those events
      // are triggered.
      $timeout(function() {
        var element = $window.document.getElementById(id);
        if(element)
          element.focus();
      });
    };
  }])
  .factory('emailService', ['$http', function($http) {
    var emailService = {
      sendFeedback: function(gameUserData, message, subject) {
        console.log("feedie");
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = $http.post("/sendfeedback?data="+gameUserData+"&message="+message+"&subject="+subject)
          .then(function(data, status, headers, config) {
            //data = {"combo_data": [{"audio_file": "http://static.saymalin.com/audio/grace_np_25.m4a", "image_file": "http://static.saymalin.com/images/non-pro/img_np57.jpg", "id": 675, "image_th_file": "http://static.saymalin.com/images/thumb/non-pro/img_np57.jpg", "text": "Spider web"}, {"audio_file": "http://static.saymalin.com/audio/char_np_186.m4a", "image_file": "http://static.saymalin.com/images/non-pro/img_np17.jpg", "id": 195, "image_th_file": "http://static.saymalin.com/images/thumb/non-pro/img_np17.jpg", "text": "Little house"}, {"audio_file": "http://static.saymalin.com/audio/char_np_1.m4a", "image_file": "http://static.saymalin.com/images/non-pro/img_np1.jpg", "id": 3, "image_th_file": "http://static.saymalin.com/images/thumb/non-pro/img_np1.jpg", "text": "hot coffee"}, {"audio_file": "http://static.saymalin.com/audio/steph_np_49.m4a", "image_file": "http://static.saymalin.com/images/non-pro/img_np41.jpg", "id": 483, "image_th_file": "http://static.saymalin.com/images/thumb/non-pro/img_np41.jpg", "text": "Yummy snack"}, {"audio_file": "http://static.saymalin.com/audio/char_np_198.m4a", "image_file": "http://static.saymalin.com/images/non-pro/img_np18.jpg", "id": 207, "image_th_file": "http://static.saymalin.com/images/thumb/non-pro/img_np18.jpg", "text": "Four deer"}, {"audio_file": "http://static.saymalin.com/audio/char_np_54.m4a", "image_file": "http://static.saymalin.com/images/non-pro/img_np6.jpg", "id": 63, "image_th_file": "http://static.saymalin.com/images/thumb/non-pro/img_np6.jpg", "text": "Mushroom "}, {"audio_file": "http://static.saymalin.com/audio/steph_np_37.m4a", "image_file": "http://static.saymalin.com/images/non-pro/img_np40.jpg", "id": 471, "image_th_file": "http://static.saymalin.com/images/thumb/non-pro/img_np40.jpg", "text": "Music store"}, {"audio_file": "http://static.saymalin.com/audio/char_np_30.m4a", "image_file": "http://static.saymalin.com/images/non-pro/img_np4.jpg", "id": 39, "image_th_file": "http://static.saymalin.com/images/thumb/non-pro/img_np4.jpg", "text": "Rainbow "}, {"audio_file": "http://static.saymalin.com/audio/sam_np_206.m4a", "image_file": "http://static.saymalin.com/images/non-pro/img_np36.jpg", "id": 423, "image_th_file": "http://static.saymalin.com/images/thumb/non-pro/img_np36.jpg", "text": "Swim laps"}], "translations": {"Swim": "Nager", "web": "toile", "Little": "Peu", "snack": "casse-cro\u00fbte", "Yummy": "D\u00e9licieux", "Mushroom": "Champignon", "Rainbow": "arc en ciel", "house": "maison", "laps": "tours", "deer": "cerf", "Spider": "Araign? e", "Four": "Quatre", "hot": "chaud", "coffee": "café", "Music": "La musique", "store": "boutique"}};
            console.log("hello");
            return data;
          }, function(data, status, headers, config) {
            console.log("nope, that didn't work");
            //redirect to 404
            return data;
          });
        // Return the promise to the controller
        return promise;
      }
    };
    return emailService;
  }])
  .factory('comboService', ['$http', function($http) {
    var comboService = {
      getCombos: function(group, round, count) {
      console.log("hey there");
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = $http.get("/combomb?group="+group+"&round="+round+"&count="+count)
          .then(function(data, status, headers, config) {
            return data;
          }, function(data, status, headers, config) {
            console.log("nope, that didn't work");
            //redirect to 404
            return data;
          });
        // Return the promise to the controller
        return promise;
      }
    };
    return comboService;
  }])
  .factory('userService', ['$http', function($http) {
    var userService = {
      saveUserPoints: function(points, reps, gainedIds, cards) {
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = $http.put("/update_user?points="+points+"&reps="+reps+"&gained="+gainedIds+"&cards="+cards, {"points" : points, "reps":  reps})
          .then(function(data, status, headers, config) {
            return data;
          }, function(data, status, headers, config) {
            //redirect to 404
            return data;
          });
        // Return the promise to the controller
        return promise;
      },
      getCurrentUser: function(callback) {
        /*if(localStorage.hasOwnProperty('currentUser')) {
          callback(JSON.parse(localStorage.getItem('currentUser')));
        }
        else {*/
          $http.get('/currentuser')
            .then(function(data, status, headers, config) {
              var proficiency_dict = {'A1':'Shift manager', 'A2':'Assistant manager', 'B1':'Middle manager', 'B2':'Regional manager', 'C1':'Vice president', 'C2':'CEO'}

              data.data.proficiency_name = proficiency_dict[data.data.proficiency];
              localStorage.setItem("currentUser", JSON.stringify(data.data));
              callback(data.data);
            }, function(data, status, headers, config) {
              //TODO DO SOMETHING BETTER HERE
              callback(data.data);
            });
        //}
      },
      getPhraseBank: function(callback) {
        $http.get('/getphrases')
          .then(function(data, status, headers, config) {
            callback(data.data);
          }, function(data, status, headers, config) {
            callback(data.data);
          });
      }
    };
    return userService;
  }]);
