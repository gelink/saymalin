# -*- coding: utf-8 -*-

from flask_admin.contrib.sqla import ModelView
from flask_security import current_user

# Create customized model view class
class SecureModelView(ModelView):
  column_display_all_relations = True
  column_exclude_list = ['password', 'login_count', 'last_login_ip', 'current_login_ip', 'last_login_at', 'words', 'translations',]

  def is_accessible(self):
    if not current_user.is_active or not current_user.is_authenticated:
      return False

    if current_user.has_role('superuser'):
      return True

    return False

  def _handle_view(self, name, **kwargs):
    """
    Override builtin _handle_view in order to redirect users when a view is not accessible.
    """
    if not self.is_accessible():
      if not current_user.has_role('superuser'):
        return redirect(url_for('home.home'))
      if current_user.is_authenticated:
        # permission denied
        abort(403)
      else:
        # login
        return redirect(url_for('security.login', next=request.url))
