# -*- coding: utf-8 -*-

from flask_security.forms import SendConfirmationForm, ResetPasswordForm, ForgotPasswordForm, RegisterForm, LoginForm, email_required, email_validator, password_required, password_length, unique_user_email, EqualTo, valid_user_email
from wtforms import SelectField, StringField, PasswordField, BooleanField, SubmitField

class ExtendedLoginForm(LoginForm):
  email = StringField(u'Adresse Email')
  password = PasswordField(u'Mot de passe')
  remember = BooleanField(u'Se souvenir de moi')
  submit = SubmitField(u'Connexion')

class ExtendedRegisterForm(RegisterForm):
  submit = SubmitField(u'Inscrivez-vous')
  email = StringField(
    u'Adresse Email',
    validators=[email_required, email_validator, unique_user_email])
  password = PasswordField(
    u'Saisissez votre mot de passe',
    validators=[password_required, password_length])
  password_confirm = PasswordField(
    u'Confirmez votre mot de passe',
    validators=[EqualTo('password', message='RETYPE_PASSWORD_MISMATCH')])
  proficiency = SelectField(u'Niveau', choices=[('A1', u'A1 : Elémentaire (découverte)'), ('A2', u'A2 : Elémentaire (survie)'), ('B1', u'B1 : Indépendant'), ('B2', u'B2 : Indépendant (avancé)'), ('C1', u'C1 : Expérimenté (autonome)'), ('C2', u'C2 : Expérimenté (maîtrise)')])

class ExtendedConfirmRegisterForm(RegisterForm):
  submit = SubmitField('inscrivez-vous')
  email = StringField(
    u'Adresse Email',
    validators=[email_required, email_validator, unique_user_email])
  password = PasswordField(
    u'Saisissez votre mot de passe',
    validators=[password_required, password_length])
  password_confirm = PasswordField(
    u'Confirmez votre mot de passe',
    validators=[EqualTo('password', message='RETYPE_PASSWORD_MISMATCH')])
  proficiency = SelectField(u'Niveau', choices=[('A1', u'A1 : Elémentaire (découverte)'), ('A2', u'A2 : Elémentaire (survie)'), ('B1', u'B1 : Indépendant'), ('B2', u'B2 : Indépendant (avancé)'), ('C1', u'C1 : Expérimenté (autonome)'), ('C2', u'C2 : Expérimenté (maîtrise)')])

class ExtendedForgotPasswordForm(ForgotPasswordForm):
  email = StringField(
    'Adresse Email',
    validators=[email_required, email_validator, valid_user_email])
  submit = SubmitField(u'Envoyer')

class ExtendedResetPasswordForm(ResetPasswordForm):
  password = PasswordField(
    u'Saisissez votre mot de passe',
    validators=[password_required, password_length])
  password_confirm = PasswordField(
    u'Confirmez votre mot de passe',
    validators=[EqualTo('password', message='RETYPE_PASSWORD_MISMATCH')])
  submit = SubmitField(u'Réinitialiser')

class ExtendedSendConfirmationForm(SendConfirmationForm):
  email = StringField(
      'Adresse Email',
      validators=[email_required, email_validator, valid_user_email])
  submit = SubmitField(u'Renvoyer')
