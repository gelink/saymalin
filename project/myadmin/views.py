from project import db, app, mail
from project.models import User
from flask import render_template, redirect, request, Blueprint, url_for
from flask_login import login_required
from flask_security import current_user
from flask_mail import Message
from datetime import datetime


myadmin_bp = Blueprint(
    'myadmin', __name__,
    template_folder='templates'
)

@myadmin_bp.route("/admin/sendreferralsummary", methods=['GET', 'POST'])
@login_required
def sendReferralSummary():
  print request.args['method']
  if not current_user.has_role('superuser'):
    return redirect(url_for('home.home'))

  flash = None
  if request.method == 'POST':
    if request.args['method'] == 'show':
      print 'poop'
      data = grabReferralObject()
      return render_template("send_referral.html", flash=flash, data=data)

    else:
      subject = 'Brainy Referral Summary'
      recipients = ['link@grace.cat']
      body = 'summ a ree summ a row'
      date = datetime.now()
      data = grabReferralData()

      msg = Message(subject=subject, recipients=recipients, body=body)
      msg.attach(filename="summary_{0}.tsv".format(date), content_type='text/tab-separated-values', data=data)
      mail.send(msg)
      flash = 'sent'

  return render_template("send_referral.html", flash=flash)

def grabReferralData():
  data = "email\tlast_active\treferred\tbitly\tproficiency\n"
  users = User.query.all()
  for user in users:
    email = user.email
    last_active = user.last_login_at
    referred = user.referral_num
    bitly = user.bitly_link
    proficiency = user.proficiency
    line = "{0}\t{1}\t{2}\t{3}\t{4}\n".format(email, last_active, referred, bitly, proficiency)
    data = "{0}{1}".format(data, line)
  return data

def grabReferralObject():
  data = []
  users = User.query.all()
  for user in users:
    data.append({'email' : user.email, 'last_active' : user.last_login_at, 'referred' : user.referral_num, 'bitly' : user.bitly_link, 'proficiency' : user.proficiency})
  return data
